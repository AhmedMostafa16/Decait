(function() {
  'use strict';
  if (typeof decait === 'undefined') {
    console.console.warn('[Decait.js DEV] Err: Decait.js package not found');
    return false;
  }

  const { r, l, list, component, mount } = decait;

  mount(
    r(
      'style',
      `.raddebug-wrap {
        position: fixed;
        top: 10px;
        right: 10px;
        bottom: 10px;
        width: 260px;
        max-width: 100%;
        z-index: 10000;
        background-color: rgba(72, 59, 106, 0.97);
        color: #fff;
        border-radius: 6px;
        padding: 14px 16px;
        box-sizing: border-box;
        overflow-x: hidden;
        overflow-y: auto;
        font-family: Tahoma;
        font-size: 16px;
      }
      .raddebug-body {
        width: 100%;
      }
      .raddebug-logo {
        display: block;
        margin: 8px 0 14px 0;
        text-align: center;
      }
      .raddebug-component {
        display: block;
        margin-top: 10px;
        background-color: rgba(44, 37, 64, 0.7);
        border-radius: 6px;
        line-height: 130%;
      }
      .raddebug-component > strong {
        display: block;
        cursor: pointer;
        padding: 5px 10px;
        border-radius: inherit;
        font-size: 14px;
        font-weight: normal;
      }
      .raddebug-component > strong > i {
        opacity: 0.2;
      }
      .raddebug-component > strong:hover {
        background-color: #2d273e;
      }
      .raddebug-component > ul {
        display: block;
        margin: 0;
        padding: 0 10px 10px 10px;
        font-size: 14px;
        list-style: none;
      }
      .raddebug-component ul li {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        color: #ae94c5;
      }
      .raddebug-component ul li > strong {
        display: inline;
        padding-left: 6px;
        color: #e2d7ff;
        font-weight: normal;
      }
    .raddebug-state-actor {
      display: block;
      margin-bottom: 10px;
    }
    .raddebug-state-actor button {
      display: block;
      width: 100%;
      margin-top: 10px;
      cursor: pointer;
      }`
    ),
    document.head
  );

  var stateActor = component({
    view: function() {
      return r(
        'div.raddebug-state-actor',
        'State Actor: ',
        cond(
          l(!this.paused),
          r(
            'button',
            {
              onclick: function() {
                this.paused = true;
                decait.freeze();
              },
            },
            '■ Pause state'
          )
        ).else(
          r(
            'button',
            {
              onclick: function() {
                decait.unfreeze();
                this.paused = false;
              },
            },
            '► Resume state'
          )
        )
      );
    },
    state: {
      _decait_dvtls: true,
      paused: false,
    },
    actions: {
      pause() {
        this.paused = true;
        decait.freeze();
      },
      resume() {
        decait.unfreeze();
        this.paused = false;
      },
    },
  });

  var data = component({
    view: function() {
      return r(
        'div',
        'Components: ',
        list(l(this.list), component => {
          // l(this.list).loop((component) => {
          return r(
            'div.raddebug-component',
            r(
              'strong',
              { onclick: this.toggle.props(component.id) },

              l(component.name ? component.name : 'unnamed-' + component.id)
            ),
            cond(
              l(this.show === component.id),
              r(
                'ul',
                cond(l(component.vars.state.length), r('strong', 'State')),
                list(l(component.vars.state), item => {
                  return r(
                    'li',
                    l(item.key),
                    ': ',
                    cond(
                      l(Array.isArray(item.value)),
                      r('strong', 'Array of ', l(item.value.length), ' items')
                    ).else(r('strong', l(item.value)))
                  );
                }),
                cond(l(component.vars.props.length), r('strong', 'Props')),
                list(l(component.vars.props), item => {
                  return r(
                    'li',
                    l(item.key),
                    ': ',
                    cond(
                      l(Array.isArray(item.value)),
                      r('strong', 'Array of ', l(item.value.length), ' items')
                    ).else(r('strong', l(item.value)))
                  );
                }),
                cond(l(component.vars.actions.length), r('strong', 'Actions')),
                list(l(component.vars.actions), item => {
                  return r(
                    'li',
                    l(item.key),
                    ': ',
                    r(
                      'button',
                      {
                        style: {
                          cursor: 'pointer',
                        },
                        onclick: () => {
                          item.value();
                        },
                      },
                      'trigger'
                    )
                  );
                })
              )
            )
          );
        })
      );
    },
    state: {
      _decait_dvtls: true,
      show: null,
      list: [],
    },
    actions: {
      onMount() {
        this.loadList(decait.activeComponents);
      },
      loadList(cp) {
        var comp = [];
        for (var ii = 0; ii < cp.length; ii++) {
          // Do not debug the debugger
          if (!cp[ii].$this._decait_dvtls) {
            var arr = {
              id: cp[ii].$this.$id,
              name: cp[ii].$this.$name,
              vars: {
                state: [],
                props: [],
                actions: [],
              },
            };
            for (var nn in cp[ii].$this.$state) {
              arr.vars.state.push({ key: nn, value: cp[ii].$this[nn] });
            }
            for (var nn in cp[ii].$this.$props) {
              arr.vars.props.push({ key: nn, value: cp[ii].$this[nn] });
            }
            for (var nn in cp[ii].$this.$actions) {
              arr.vars.actions.push({ key: nn, value: cp[ii].$this[nn] });
            }
            comp.push(arr);
          }
        }
        // console.log(comp)
        this.list = comp;
        setTimeout(() => {
          this.loadList(decait.activeComponents);
        }, 10);
      },
      toggle(id) {
        this.show = this.show === id ? null : id;
      },
    },
  });

  mount(
    r(
      'div.raddebug-wrap',
      r(
        'div.raddebug-body',
        r(
          'div.raddebug-logo',
          r('img', {
            src: LOGO,
            width: 107,
          })
        ),
        r('div', new stateActor()),
        r('div', new data())
      )
    ),
    document.body
  );
})();
