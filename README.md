# Decait

#### Small in size front-end framework.

[![npm version](https://img.shields.io/npm/v/decait.svg?style=flat-square)](https://www.npmjs.com/package/decait)
[![npm downloads](https://img.shields.io/npm/dm/decait.svg?style=flat-square)](https://www.npmjs.com/package/decait)
[![gzip bundle size](http://img.badgesize.io/https://unpkg.com/decait@latest/dist/decait.min.js?compression=gzip&style=flat-square)](https://unpkg.com/decait@latest/dist/decait.js)

It's built quite differently from any other framework. It doesn't use any kind of diffing algorithm nor virtual Dom which makes it really fast.

With Decait you can create any kind of single-page applications or more complex applications.

## Installation

By NPM:

```
npm install decait --save-dev
```

or

By Yarn:

```
yarn add decait --dev
```

### Browser Compatibility

Decait currently is compatible with browsers that support at least ES5.

### Ecosystems

> Coming soon

### Documents
> Coming soon

## License

Copyrights © 2018 [Tremium](www.github.com/Tremium).

MIT License.
